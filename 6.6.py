'''
Сделайте класс User, в котором будут следующие поля - name (имя), age (возраст), методы setName, getName, setAge, getAge.
Сделайте класс Worker, который наследует от класса User и вносит дополнительное поле salary (зарплата), а также методы getSalary и setSalary.
Создайте объект этого класса 'Иван', возраст 25, зарплата 1000. Создайте второй объект этого класса 'Вася', возраст 26, зарплата 2000. Найдите сумму зарплата Ивана и Васи.
Сделайте класс Student, который наследует от класса User и вносит дополнительные  поля стипендия, курс, а также геттеры и сеттеры для них.
Сделайте класс Driver (Водитель), который будет наследоваться от класса Worker из предыдущей задачи. Этот метод должен вносить следующие поля: водительский стаж, категория вождения (A, B, C).
'''

class User:
    def __init__(self,name = 'Unknown', age = 'Unknown'):
        self.name = name
        self.age = age

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def setAge(self, age):
        self.age = age

    def getAge(self):
        return self.age

user = User()
print(user.getName())
user.setName('Vasya')
print(user.getName())
print(user.getAge())
user.setAge(27)
print(user.getAge())

class Worker(User):
    def __init__(self, name, age, salary = 'Unknown'):
        super().__init__(name, age)
        self.salary = salary

    def setSalary(self, salary):
        self.salary = salary

    def getSalary(self):
        return self.salary

Ivan = Worker('Ivan', 25, 1000)
Vasya = Worker('Vasya', 26, 2000)
print(Ivan.getSalary() + Vasya.getSalary())

class Student(User):
    def __init__(self, name, age, stipend = 'Unknown', course = 'Unknown' ):
        super().__init__(name, age)
        self.stipend = stipend
        self.course = course

    def setStipend(self, stipend):
        self.stipend = stipend

    def getStipend(self):
        return self.stipend

    def setCourse(self, course):
        self.course = course

    def getCourse(self):
        return self.course

Petya = Student('Petya', 22, 5200, 3)
print(Petya.getStipend())
print(Petya.getCourse())
Petya.setStipend(666)
Petya.setCourse(4)
print(Petya.getStipend())
print(Petya.getCourse())

class Driver(Worker):
    def __init__(self, name, age, salary, experience = 0, licence = 'Unknown'):
        super().__init__(name, age, salary)
        self.experience = experience
        self.licence = licence

    def setExperience(self, experience):
        self.experience = experience

    def getExperience(self):
        return self.experience

    def setLicence(self, licence):
            self.licence = licence

    def getLicence(self):
            return self.licence

Grisha = Driver('Grisha', 21, 15000, 2, 'B')
print(Grisha.getExperience())
print(Grisha.getLicence())
Grisha.setLicence('A')
print(Grisha.getLicence())