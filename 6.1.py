'''
Реализацию встроенной функции len: функция принимает список, возвращает его длину
'''

def len_emul(a):
    s = 0
    for i in a:
        s += 1
    return s

test = ['1','2','3', '27']
print(len_emul(test))