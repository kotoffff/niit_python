'''
ф-ию to_title: принимает строчку, ищет пробелы, первые буквы после них делает заглавными:
'''

def to_title(a):
    a = a.split()
    arr = []
    for i in a:
        arr.append(i.capitalize())
    a = ' '.join(arr)
    return a

test = "nikita Ladoshkin evgenyevich"
print(to_title(test))