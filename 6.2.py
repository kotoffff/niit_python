'''
Реализацию функции reversed: ф-ия принимает список, возращает его, но элементы расположенны в обратном порядке
'''

def rev_emul(a):
    s = len(a) -1
    arr_rev = []
    while s >= 0:
        arr_rev.append(a[s])
        s -= 1
    return arr_rev

test = ['1', '2', '3', '4', '5']
print(rev_emul(test))