#lesson work
'''
Лекция 3
Написать функцию is_prime, принимающую 1 аргумент  число от 0 до 1000, и возвращающую True, если оно простое, и False - иначе.
'''
def is_prime(dig):
    if dig.isdigit() == True:
            dig = int(dig)
    else:
        print("Incorrect value. Please start again and enter digit from 1 to 1000")

    if  dig == 1:
        print("False")
    if dig == 2:
        print("True")
    elif 2 < dig <= 1000:

        i = 2
        while i < dig:
            if dig % i == 0:
                print("False")
                quit()
            i += 1
        print("True")

    else:
        print("Incorrect value. Please start again and enter digit from 1 to 1000")

is_prime(str(1001))

'''
Лекция 3
ф-ию abs(x) - Возвращает абсолютную величину (модуль числа).
'''
def module(x):
        print(abs(x))
module(-3)

'''
Лекция 3
Написать ф-ию wave, которая будет принимать строку, а возвращать список строк с такими же символами, но один из символов будет заглавным
wave("hello") => ["Hello", "hEllo", "heLlo", "helLo", "hellO"]
'''
def wave(x):
    q = []
    for i in range(len(x)):
        s = list(x)
        s[i] = x[i].upper()
        q.append("".join(s))
    print(q)

wave("hello")

##########
#homework
###########

import time

'''
Лекция 3
Написать декоратор timeit, которые с помощью метода time библиотеки time  будет выводить вам время исполнения​ ф-ии по её завершению
'''
import time

def timeit(f):
    def wrap(n):
       start=time.time()
       f(n)
       stop=time.time()
       print ('Время выполнения:',stop-start,'секунд')
    return wrap

@timeit
def spow(n):
    print (2**n)

n=int (input("какая степень двойки Вас интересует:"))
spow(n)

'''
Лекция 3
Написать ф-ию factorial, которая принимает число, а возвращает его факториал
'''
def factorial(n):
    if n==0:
        return 1
    else:
        return n * factorial(n-1)
print("Hi it's factorial function \nenter your number and i will return factorial")
print("For exit enter q")
number = input("Yor number: ")

if number == 'q' or number == "Q":
    print("Good bay!")
elif number.isdigit()!= False:
    num_int =int(number)
    print(factorial(num_int))
else:
    print("What???????!!!")