class BaseNode:
    def __init__(self,name):
        self.name=name

    def __repr__(self):
        pass

class File(BaseNode):
    def __init__(self,file):
        self.file=file

    def __repr__(self):
        return 'file:"{}"'.format(self.file)

class Dir(BaseNode):
    def __init__(self,nameofdir): #создается строка с нулевым элементом=названию
        a=list()
        a.append(nameofdir)
        self.nameofdir=a

    def __repr__(self):
        for i in range (0,len(self.nameofdir)):
            s=(len(self.nameofdir))
            if i==0:
                output = 'directory:{}('.format(self.nameofdir[0])
            elif type(self.nameofdir[i])==File:
                output=output+','*bool(i>1 and i<s)+str(self.nameofdir[i])
            elif type(self.nameofdir[i])==Dir:
                output=output+','*bool(i>1 and i<s)+repr(self.nameofdir[i])
            if len(self.nameofdir) == 1:
                output = output + 'empty'

        return output+')'


    def add(self, smth):
       self.nameofdir.append(smth)

    def remove(self,smth):
        self.nameofdir.remove(smth)




d1 = Dir('dir1') # папка на верхнем уровне
d1.add(File('text1.txt'))
d2 = Dir('dir2')
d1.add(d2)
d3 = Dir('dir3')
d1.add(d3)
d1.remove(d3) # пример удаления вложенной структуры
d4 = Dir('dir4')
d2.add(d4)
d4.add(File('image1.txt'))
d4.add(File('text2.txt'))
d1.add(File('paper1.pdf'))
d2.add(Dir('dir5'))


print(d1)
